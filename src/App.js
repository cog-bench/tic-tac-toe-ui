import React, { useState } from 'react';
import './App.css';

function App() {
  const [buttonText, setButtonText] = useState("Click me...");
  function handleClick(ev) {
    if(buttonText === "Click me...") {
      ev.preventDefault();
      console.log("Clicked me");
      fetch('http://35.222.65.146/api')
        .then(response => response.text())
        .then(result => setButtonText(result))
        .catch(console.log);
    }
  }
  return (
    <div className="App">
      <header className="App-header">
        <button className="click-me" alt="test button" onClick={handleClick}>{buttonText}</button>
      </header>
    </div>
  );
}

export default App;
