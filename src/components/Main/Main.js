import React from 'react';
import PropTypes from 'prop-types';
import './Main.css';

const Main = () => (
  <div className="Main" data-testid="Main">
    Main Component
  </div>
);

Main.propTypes = {};

Main.defaultProps = {};

export default Main;
